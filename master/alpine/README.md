## Dockerfile For GNUKhata Based on Alpine

Installs GNUKhata `v6.50`

### From docker hub

`docker pull kskarthik/gnukhata:6.50-alpine`

https://hub.docker.com/r/kskarthik/gnukhata
